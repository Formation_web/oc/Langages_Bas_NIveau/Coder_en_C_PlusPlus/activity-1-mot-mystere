#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <fstream>

using namespace std;

string melangerLettres(string mot) {
    string melange;
    int position(0);

    //Tant qu'on n'a pas extrait toutes les lettres du mot
    while (mot.size() != 0) {
        //On choisit un numÃ©ro de lettre au hasard dans le mot
        position = rand() % mot.size();
        //On ajoute la lettre dans le mot mÃ©langÃ©
        melange += mot[position];
        //On retire cette lettre du mot mystÃ¨re
        //Pour ne pas la prendre une deuxiÃ¨me fois
        mot.erase(position, 1);
    }

    //On renvoie le mot mÃ©langÃ©
    return melange;
}


int CountStrings()
{

    string line ;
    ifstream dictionaire("dico.txt");

    int nStrings = 0;
    while (getline(dictionaire, line ) )
        ++nStrings;

    return nStrings;
}

int randLigne(int min,int max) {
    int randLigne = 0;
    randLigne = rand()%(max-min + 1) + min;

    return randLigne;
}

int main() {
    string motMystere, motMelange, motUtilisateur;
    char reponseUtilisateur;
    string ligne;
    int randLigne(0);
    int nbrLine(0);
    int min(0);

    //Initialisation des nombres alÃ©atoires
    srand(time(0));

    do { // Faire tant que l'utilisateur dit oui

        int i(0);

        //1 : On demande de saisir un mot (cas saisie manuelle)
        //        cout << "Saisissez un mot" << endl;
        //        cin >> motMystere;

        /*
         *
         * Bien preciser le chemin absoulu vers l'endroit du fichier (pour ceux qui vont corriger)
         * C'est surement pas le meme chemin que j'utilise
         *
         */

        ifstream dictionaire("D:/motMystere/dico.txt");

        // Compte le nombre de lignes dans le fichier
        nbrLine = CountStrings() - 1;
        // On choisit une ligne au hasard
        randLigne = ::randLigne(min,nbrLine);

        if(dictionaire)  //On teste si tout est OK
        {
            //Tout est OK, on peut utiliser le fichier
            while( getline( dictionaire, ligne ))
            {
                if(i == randLigne )
                {
                    // on prend une ligne au hasard et ce sera notre mot mystere
                    motMystere = ligne;
                }
                i++;
            }
        }
        else
        {
            cout << "ERREUR: Impossible d'ouvrir le fichier." << endl;
        }

        // Declare le nombre de coups pour trouver le mot 5
        int nbrEssais(5);

        //2 : On rÃ©cupÃ¨re le mot avec les lettres mÃ©langÃ©es dans motMelange
        motMelange = melangerLettres(motMystere);

        //3 : On demande Ã  l'utilisateur quel est le mot mystÃ¨re
        do {
            cout << endl << "Quel est ce mot ? " << motMelange << endl;
            cin >> motUtilisateur;


            if (motUtilisateur == motMystere) {
                cout << "Bravo vous avez trouvé !" << endl;
            } else {
                cout << "Ce n'est pas le bon mot !" << endl;

                nbrEssais--;

                cout << "Il vous reste : " << nbrEssais << " essais" << endl;

                if (nbrEssais == 0) {
                    cout << "Vous avez perdu. Le mot est : " << motMystere << endl;
                }
            }
        } while (motUtilisateur != motMystere && nbrEssais != 0);  //On recommence tant qu'il n'a pas trouvé



        // On demande si utilisateur veut recommencer
        cout << "Voulez vous recommencer ? : o/n" << endl;
        cin >> reponseUtilisateur;
        // Verification sur la rentrée de l'utilisateur
        while (reponseUtilisateur != 'o' && reponseUtilisateur != 'n') {
            cout << "Veuillez saisir o (pour oui) ou n (pour non)" << endl;
            cout << "Voulez vous recommencer ? : o/n" << endl;
            cin >> reponseUtilisateur;
        }

    } while (reponseUtilisateur == 'o');

    cout << "A bientot" << endl;

    return 0;
}
