/*
 * motDico.h
 *
 *  Created on: 21 oct. 2018
 *      Author: moroo
 */

#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <cstdlib>

/*
 * M�thode comptant le nombre de mots dans dico.txt
 */
int nombreMots();

/*
 * M�thode permettant de piocher un mot au hasard dans l'ensemble du dictionnaire choisi
 */
std::string piocheMot(int nbMots);
