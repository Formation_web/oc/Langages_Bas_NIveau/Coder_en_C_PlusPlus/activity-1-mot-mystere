/*
 * interactionsUtilisateurs.h
 *
 *  Created on: 21 oct. 2018
 *      Author: moroo
 */

#ifndef HEADER_INTERACTIONSUTILISATEURS_H_
#define HEADER_INTERACTIONSUTILISATEURS_H_

#include <iostream>
#include <string>

/*
 * M�thode impl�mentant le choix d'un mot � deviner par un autre utilisateur
 */
std::string choixUtilisateur1();

/*
 * M�thode comparant les tentatives du joueur sur 5 essais maximum si le mot n'est pas
 * trouv� du premier coup
 */
void tentativeUtilisateur2(std::string mot, std::string melange);

/*
 * Permet de convertir le mot tapez par le joueur dans la m�me casse que les mots
 * de dico.txt
 */
void convertUpper(std::string& s);

#endif /* HEADER_INTERACTIONSUTILISATEURS_H_ */
