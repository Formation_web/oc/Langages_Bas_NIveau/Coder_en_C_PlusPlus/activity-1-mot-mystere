/*
 * melangeMot.h
 *
 *  Created on: 21 oct. 2018
 *      Author: moroo
 */

#ifndef HEADER_MELANGEMOT_H_
#define HEADER_MELANGEMOT_H_

#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>

/*
 * M�thode m�langeant les lettres du mot pass� en param�tre
 */
std::string melangeMot(std::string mot);

#endif /* HEADER_MELANGEMOT_H_ */
