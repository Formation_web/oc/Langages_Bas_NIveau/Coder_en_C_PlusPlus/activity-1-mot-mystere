/*
 * main.cpp
 *
 *  Created on: 21 oct. 2018
 *      Author: moroo
 */


#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <cstdlib>
#include "../header/interactionsUtilisateurs.h"
#include "../header/melangeMot.h"
#include "../header/motDico.h"

using namespace std;

int main(){
	char jeu = 'o'; // 'o/N'

	srand(time(0));

	cout << "*** Bienvenue dans le jeu du mot myst�re ***" << endl;
	do {
		//1 : On demande de saisir un mot
		//string const mot = choixUtilisateur1();
		string const mot = piocheMot(nombreMots());

		//2 : On m�lange les lettres du mot
		//string melange = "melange";
		string melange = melangeMot(mot);

		//3 : On demande � l'utilisateur quel est le mot myst�re
		tentativeUtilisateur2(mot,melange);

		//4 : On demande � l'utilisateur s'il veut faire une nouvelle partie
		cout << "Voulez-vous faire une autre partie ? (o/N) " << endl;
		cin  >> jeu;

		if (jeu == 'N'){
			cout << "*** Au revoir ! ***" << endl;
		}

	} while (jeu == 'o');

	return 0;
}
