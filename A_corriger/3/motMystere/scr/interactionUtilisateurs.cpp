/*
 * interactionUtilisateurs.cpp
 *
 *  Created on: 21 oct. 2018
 *      Author: moroo
 */

#include "../header/interactionsUtilisateurs.h"

std::string choixUtilisateur1(){
	std::string mot;
	std::cout << "Saisissez un mot : " << std::endl;
	std::cin >> mot;

	//On tape des lignes vide dans la console
	for (int i=0; i<10; ++i){
		std::cout << "" << std::endl;
	}
	convertUpper(mot);
	return mot;
}

void tentativeUtilisateur2(std::string mot, std::string melange){
	std::string motTentative("tentative");

	int nbTentative = 0;

	while (motTentative != mot){
		std::cout << "Il vous reste " << 5-nbTentative << " essais." << std::endl;
		std::cout << "Quel est ce mot ? " << std::endl;
		std::cout << melange << std::endl;

		std::cin >> motTentative;
		convertUpper(motTentative);
		//std::cout << motTentative << std::endl;
		if (motTentative != mot){
			std::cout << "Ce n'est pas ce mot !" << std::endl;
		}
		else {
			std::cout << "Bravo !" << std::endl;
		}
		++nbTentative;
		if (nbTentative == 5){
			std::cout << "Dommage! Le mot a trouver �tait : " << mot << std::endl;
			break;
		}
	}
}

void convertUpper(std::string& s){
	for (unsigned int i=0; i<s.length(); ++i){
		s[i] = toupper(s[i]);
	}
}

